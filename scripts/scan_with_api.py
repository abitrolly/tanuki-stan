from label_classifier import Issue, LabelClassifier

from datetime import date, timedelta
from dateutil import parser
from urllib.parse import quote_plus
import argparse
import json
import os
import requests
import sys
import yaml


SLACK_URL = os.environ.get('SLACK_URL', None)
PRODUCTION_MODE = os.environ.get('PRODUCTION_MODE', False) == "1"
PRIVATE_TOKEN = os.environ.get('PRIVATE_TOKEN', None)

if PRODUCTION_MODE and PRIVATE_TOKEN is None:
    print('PRIVATE_TOKEN is not set!')
    sys.exit(1)


def send_slack_message(message):
    if SLACK_URL is None:
        return True

    data = {'text': message, 'channel': '@stanhu', 'username': 'TanukiStan', 'icon_emoji': ':tanuki'}

    if PRODUCTION_MODE:
        data['channel'] = 'feed_tanuki-stan'

    headers = {'Content-Type': 'application/json'}
    resp = requests.post(SLACK_URL, headers=headers, data=json.dumps(data))
    print("Slack message response: %s" % resp)


def update_issue_with_note(issue, label, confidence):
    note = '''This issue was automatically tagged with the label ~"{label}" by [TanukiStan](https://gitlab.com/gitlab-org/ml-ops/tanuki-stan), a machine learning classification model, with a probability of {confidence:.2g}.

If this label is incorrect, please tag this issue with the correct [group](https://about.gitlab.com/handbook/product/categories/#devops-stages) label as well as ~"automation:ml wrong" to help TanukiStan learn from its mistakes.

If you are unsure about the correct group, please **do not** leave the issue without a group label. Please refer to GitLab's [shared responsibility functionality](https://about.gitlab.com/handbook/product/categories/#shared-responsibility-functionality) guidelines for more information on [how to triage this kind of issues](https://about.gitlab.com/handbook/engineering/quality/issue-triage/#shared-responsibility-issues).

Authors who do not have permission to update labels can leave the issue to be triaged by group leaders initially assigned by [TanukiStan](https://gitlab.com/gitlab-org/ml-ops/tanuki-stan)

*This message was generated automatically.
You're welcome to [improve it](https://gitlab.com/gitlab-org/ml-ops/tanuki-stan/-/blob/master/scripts/scan_with_api.py).*

/label ~"{label}" ~"automation:ml"
''' \
    .format(label=label, confidence=confidence)

#    print(note)

    labels = issue['labels'] + [label, "automation:ml"]
    iid = issue['iid']

    print("adding labels %s" % labels)

    if PRIVATE_TOKEN is None:
        return True

    post_note_url = "https://docker.for.mac.localhost:3001/api/v4/projects/1/issues/35/notes"

    if PRODUCTION_MODE:
        post_note_url = "https://gitlab.com/api/v4/projects/278964/issues/%d/notes" % iid

    headers = {'PRIVATE-TOKEN': os.environ['PRIVATE_TOKEN'], 'Content-Type': 'application/json'}

    data = {'body': note}
    resp = requests.post(post_note_url, data=json.dumps(data), headers=headers, verify=False)

    if resp.status_code != 201:
        print("Failed labelling and adding note: %s" % resp)
        return False

    return True


def request_issues(created_after, page):
    per_page = 100
    forbidden_labels = ["automation:ml", "Engineering Productivity", "group::*"]
    not_labels = quote_plus(",".join(forbidden_labels))
    url = "https://gitlab.com/api/v4/projects/278964/issues?state=opened&not[labels]={}&per_page={}&created_after={}&page={}".format(not_labels, per_page, created_after.isoformat(), page)
    print(url)
    resp = requests.get(url)

    unlabeled_issues = []

    json_data = resp.json()
    more_issues = len(json_data) == per_page

    for issue in json_data:
        title = issue['title']
        labels = issue['labels']

        lower_title = title.lower()
        if lower_title.find('live') >= 0 or lower_title.find('watch') >= 0 or lower_title.find('reddit') >= 0:
            print("Skipping potential spam: %s" % title)
            continue

        unlabeled_issues.append(issue)

    return unlabeled_issues, more_issues


def tag_issues_by_date(classifier, created_after, page):
    unlabeled_issues, more_issues = request_issues(created_after=created_after, page=page)

    if len(unlabeled_issues) == 0:
        print("No unlabeled issues")
        return

    issues = [Issue(x['title'], x['description']) for x in unlabeled_issues]
    predictions = classifier.predict_labels_for_issues(issues)

    for i, (label, confidence) in enumerate(predictions):
        issue = unlabeled_issues[i]
        iid = issue['iid']
        url = "https://gitlab.com/gitlab-org/gitlab/issues/%d" % iid

        if confidence > 0.20:
            message = ":green-circle-check: <%s|Issue #%d: %s>: :arrow_right: added label `%s` (probability %.2f)" % (url, iid, issue['title'], label, confidence)
            print(message)
            success = update_issue_with_note(issue, label, confidence)
            if success:
                send_slack_message(message)
        else:
            message = ":red_circle: <%s|Issue #%d: %s>: did not apply label `%s` (probability %.2f)" % (url, iid, issue['title'], label, confidence)
            print(message)
            send_slack_message(message)

    return more_issues


def tag_issues(start_date, labels_deny_list):
    page = 1
    classifier = LabelClassifier(labels_deny_list)

    while True:
        more_issues = tag_issues_by_date(classifier=classifier,
                                         created_after=start_date,
                                         page=page)
        if not more_issues:
            break
        page += 1


def setup_args():
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument(
        '--start-date', default=(date.today() - timedelta(days=1)).isoformat(),
        help="date from which to start scanning issues (e.g. 2021-04-01), default: yesterday",
        type=str)
    parser.add_argument(
        '--config', default="config.yml",
        help="Config file that specifies classifier behavior (deny list for labels)",
        type=str)

    return parser.parse_args()


def load_config(filename):
    with open(filename, "r") as stream:
        config = yaml.safe_load(stream)
        try:
            return config['labels']['deny']
        except KeyError:
            return []


args = setup_args()
start_date = parser.parse(args.start_date)
labels_deny_list = []

if args.config:
    labels_deny_list = load_config(args.config)

tag_issues(start_date, labels_deny_list)
