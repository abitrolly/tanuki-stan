import tensorflow as tf
import numpy as np

from bs4 import BeautifulSoup
from collections import namedtuple

from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing.sequence import pad_sequences

from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize

Issue = namedtuple('Issue', ('title', 'description'))


class LabelClassifier:
    MAX_TEXT_LENGTH = 8192
    PADDING_TYPE = 'post'
    TRUNC_TYPE = 'post'
    STOP_WORDS = set(stopwords.words('english'))

    def __init__(self, labels_deny_list):
        self.labels_deny_list = labels_deny_list
        self.load_models()

    def load_models(self):
        self.model = load_model('../models/gitlab_issue_classify.h5')

        with open('../models/text_tokenizer.json') as f:
            self.text_tokenizer = tf.keras.preprocessing.text.tokenizer_from_json(f.read())

        with open('../models/label_tokenizer.json') as f:
            self.label_tokenizer = tf.keras.preprocessing.text.tokenizer_from_json(f.read())
            self.label_word_index = self.label_tokenizer.word_index
            self.index_to_label_dict = {v: k for k, v in self.label_word_index.items()}

    def tokenize(self, title, description):
        text = "%s\n\n%s" % (title, description)
        text = BeautifulSoup(text, features="html.parser").get_text().lower()
        word_tokens = word_tokenize(text)
        word_tokens = [word.lower() for word in word_tokens if word.isalpha()]
        text = ' '. join(word_tokens)

        return text

    def predict_labels_for_issues(self, issues):
        '''Given an array of issues, predict their lables. Returns an array of tuples
        containing (label, confidence).
        '''
        texts = [self.tokenize(issue.title, issue.description) for issue in issues]
        new_seq = self.text_tokenizer.texts_to_sequences(texts)
        padded = pad_sequences(new_seq,
                               maxlen=self.MAX_TEXT_LENGTH,
                               padding=self.PADDING_TYPE,
                               truncating=self.TRUNC_TYPE)
        predictions = self.model.predict(padded)

        data = []

        for i, result in enumerate(predictions):
            while True:
                # Find the best match and look up label for that
                index = np.argmax(result)
                confidence = result[index]
                label = self.index_to_label_dict[index].replace('_', ' ')

                # Try to find the next best prediction if the current label is
                # not allowed
                if label in self.labels_deny_list:
                    print("Label '%s' is in deny list, trying to find another match" % label)
                    result[index] = 0
                    # Try to find the next best match
                    continue
                else:
                    break

            data += [(label, confidence)]

        return data

# Example run:
# labeler = LabelClassifier()
# issue = Issue("import failed", "This is a new project import")
# issue2 = Issue("Geo: repository sync not working", "")
#
# print(labeler.predict_labels_for_issues([issue, issue2]))
